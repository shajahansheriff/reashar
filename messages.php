<?php
   
    require_once("worker/cake/models/config.php");
    if(!isUserLoggedIn()) { header("Location: index.php"); die(); }

    //$username = $loggedInUser->username;
    $user_id = $loggedInUser->user_id;
    //foreach ($userdetails as $key => $value) {
    // echo $key."=>".$value;
   //}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Note there is no responsive meta tag here -->

    <link rel="shortcut icon" href="img/favicon.png">

    <title>Welcome | Reashar</title>

    <!-- Bootstrap core CSS -->
    <link href="css/global.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/non-responsive.css" rel="stylesheet">
    <link href="css/animate-custom.css" rel="stylesheet" />
    <link href="css/loginbox.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
     <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body style="background-color:#f1f1f1;">

    <?php 
    include'navbar.php' ?>
    <?php echo '<div id ="lu" style ="display:none;">'.$loggedInUser->user_id.'</div>'; ?>
   
  
    
    <div class="container" >
 <div class="col-xs-1"></div>
   <div class="col-xs-4" >
        <div class="chat-panel panel panel-default">
                        <div class="panel-heading"><span class="pull-right" style = "cursor:pointer">New Message</span>
                            <i class="fa fa-envelope fa-fw"></i>
                            Messages
                            
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body" >
                            <ul class="chat" id = "recepient_list">
                                
                                
                                
                            </ul>
                        </div>
                    </div>
     </div>   
 
  <div class="col-xs-6">
    
                              <div class="chat-panel panel panel-default" id = "msg" style = "display:none">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-fw"></i>
                            Chat
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-chevron-down"></i>
                                </button>
                                <ul class="dropdown-menu slidedown">
                                    <li>
                                        <a href="#" id="refresh_btn">
                                            <i class="fa fa-refresh fa-fw"></i> Refresh
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" id = "delete_btn">
                                            <i class="fa fa-times-circle fa-fw" ></i>Delete All
                                        </a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body" ><span id ="msg_to" style = "display:none">1000000017</span>
                            <ul class="chat refresh" >No  Messages Yet
                            </ul>
                        </div>
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                         
                                            
                                          
                                            <textarea id="message_body" maxlength="500" class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                        
                                        <span class='counter_msg'></span><br>
                                        <input type="submit" class="btn btn-primary chat-button" value="Reply" id = "submit_message"></input>
                        </div>
                        
                    </div>
                            
<div class="chat-panel panel panel-default" id = "new_message">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-fw"></i>
                            New Message
                            
                        </div>
                        <div class="panel-body">
                          <form action="#" method="post">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="msgto" placeholder="Message to:">
                                        </div>
                                        
                                        <div>
                                            <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                        </div>
                                        <input type="submit" class="btn btn-primary chat-button"  ></input>
                                    </form>
                        </div>
  </div>

    </div> <!-- /container -->
   



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
      <script type="text/javascript" src="js/timeago.js"></script>
  
    <script type="text/javascript" src="js/script.js"></script>
    <script src="js/global.min.js"></script>
    <script src="js/jqueryfxns.js"></script>
    <script language="javascript" src="js/jquery-1.2.6.min.js"></script> 
        <script language="javascript" src="js/jquery.timers-1.0.0.js"></script>
        <script src="js/timezone.js"></script>
        <script type="text/javascript">

         var every = function()
            {
              var msg_to = parseInt($('#msg_to').html());
                    var timezone = jstz.determine().name(); 
                    if(!msg_to) alert("somthing null you idiot  :(");
                         else
                         { 
                    $.ajax({
                      url: "worker/refresh.php",
                      type: "POST",
                      cache: false,
                      data: {msg_to: msg_to, timezone: timezone},
                      success: function(html){
                        $(".refresh").html(html);$(".timeago").timeago();
                        /*var $t = $('.refresh');                 //To scroll down the division
            $t.animate({"scrollTop": $('.refresh')[0].scrollHeight}, "slow");*/
                      }
                    })
                    $.ajax({
                      url: "worker/refresh2.php",
                      type: "POST",
                      cache: false,
                      data: {functionname:""},
                      success: function(datax){
                        //$("#recepient_list").html(html);
                        $(".unseen_count").html("");
                        datax = JSON.parse(datax);
                        $(".unseen_count").html("");
                        for(var i=0;i<=datax['id'].length;i++)
                        {$obj ="#"+datax['id'][i];
                          $($($obj)).find(".unseen_count").html(datax['unseen_count'][i]);
                        }
                      }
                    })
                     

                  }

                
            


            }


        $('#msg_to').ready(function(){
           var j = jQuery.noConflict();
            $('#msg_to').ready(function()
            {every();
                j.ajax({
                      url: "worker/refresh2.php",
                      type: "POST",
                      cache: false,
                      data: {functionname: "messaged_users"},
                      success: function(html){
                        j("#recepient_list").html(html);
                      }
                    })

              
                j(".refresh").everyTime(5000,function(i){
                    var msg_to = parseInt(j('#msg_to').html());
                    var timezone = jstz.determine().name(); 
                    if(!msg_to) alert("somthing null you idiot  :(");
                         else
                         { 
                    j.ajax({
                      url: "worker/refresh.php",
                      type: "POST",
                      cache: false,
                      data: {msg_to: msg_to, timezone: timezone},
                      success: function(html){
                        j(".refresh").html(html);$(".timeago").timeago();
                      }
                    })
                    $.ajax({
                      url: "worker/refresh2.php",
                      type: "POST",
                      cache: false,
                      data: {functionname:""},
                      success: function(datax){
                        //j("#recepient_list").html(html);
                        $(".unseen_count").html("");
                        datax = JSON.parse(datax);
                        for(var i=0;i<=datax['id'].length;i++)
                        {
                          
                          $obj ="#"+datax['id'][i];
                          $($($obj)).find(".unseen_count").html(datax['unseen_count'][i]);
                        }
                      }
                    })
                     

                  }
                })
                
            });
        $(document).on('click', '[name="friend"]', function (e) {
            e.preventDefault;
             $(".refresh").html("");
            $('#msg').css("display","block");
            $('#new_message').css("display","none");
            $('#')

            $("#msg_to").html($(this).attr("id"));

            every();
        });




         $(document).on('click', '#refresh_btn', function (e) {
            e.preventDefault;
            every();
        });




         $(document).on('click', '#delete_btn', function (e) {
            e.preventDefault;
            var msg_to = parseInt($('#msg_to').html()); 
            var obj ="#"+msg_to;
            $(obj).remove();
            $('#msg').css("display","none");
            $('#new_message').css("display","block");
              j.ajax({
                      url: "worker/refresh2.php",
                      type: "POST",
                      cache: false,
                      data: {msg_to: msg_to, functionname: "delete_messages"},
                      success: function(html){
                      }
                    })
              every();
          
             every();
           
        });

            $("#msg").click(function()
                        {         
                             var msg_to = parseInt($('#msg_to').html());
                             var timezone = jstz.determine().name(); 
                    
                    j.ajax({
                      url: "worker/save.php",
                      type: "POST",
                      cache: false,
                      data: {msg_to: msg_to, timezone: timezone, functionname: "seen"},
                      success: function(html){
                        $(".refresh").html(html);$(".timeago").timeago();
                      }
                    });

             });
            $(document).on("change", "#msg_to", function()
            {
                   
                             every();
                
                
            });
            $(document).on('focus keyup', '#message_body', function (e) {
          
            var $this = $(this);
            var msgSpan = $this.siblings('.counter_msg');
            var ml = parseInt($this.attr('maxlength'), 10);
            var length = $this.val().length;
            var msg = ml - length + ' characters of ' + ml + ' characters left';

            msgSpan.html(msg);
        });

            $('#submit_message').click(function() {
                        var text = $('#message_body').val();

                        var msg_to = parseInt($('#msg_to').html());

                        if(!text || !msg_to)
                            alert("somthing null :(");
                        else
                        { 
                            j.ajax({                
                                 url: "worker/save.php",
                                 type: "POST",
                                 data: {text: text,msg_to: msg_to},
                                 success: function(msg)
                                             {     
                                                   // $("#content").append('<center><br><a href ="#" id="read more" class = "">Read More</a></center>');
                                                   $('.refresh')
                                                   $('#message_body').val("");
                                                   $('.counter_msg').html("");
                                                    every();
                                             }
                                    });
                        }
            });
            
                   
                
           $('.refresh').css({color:"green"});
        });
        </script>
            <script type="text/javascript">
              $(function() {
                $('#asd').click(function() {
                $(".media-body").toggle(20);
                });
              });
           
            </script>
    
    
    
   
    
    
  </body>

</html>
