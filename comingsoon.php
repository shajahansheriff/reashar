<?php

    require_once("worker/cake/models/config.php");  
    if(!isUserLoggedIn()) { header("Location: index.php"); die(); }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
     <title>Upload Book</title>
    <?php require_once 'web_css.php';?>
  </head>

  <body>

    <?php include'navbar.php' ?>
   
  
    
    <div class="container">
 
   <div class="col-xs-2">
        
    </div>


       <div class="col-xs-8 ">

    
         <div class="panel panel-default upload-div" id="main_details" >
              <div class="panel-heading"><h5>Sorry! for inconvenience</h5></div>
                  <div class="panel-body">
                      <div class="media"><!-- Needed For Uploading books -->
                  
                            <div class="media-body" >
                               
                                <h5>
                                  The Website is in Beta mode. This feature will be available soon in few days. Check later <b>:)</b></h5> <br>
                                <a href="home.php">Back to home</a> 

                            
                  </div>
                  </div>
          </div>                     
                    
          

    </div> <!-- /container -->



    <?php require_once'web_js.php'; ?>
    
  </body>
</html>
