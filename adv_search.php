    <?php

    require_once("worker/cake/models/config.php");  
    if(!isUserLoggedIn()) { header("Location: index.php"); die(); }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Note there is no responsive meta tag here -->

    <link rel="shortcut icon" href="img/favicon.png">

    <title>Welcome | Reashar</title>

    <!-- Bootstrap core CSS -->
    <link href="css/global.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/non-responsive.css" rel="stylesheet">
    <link href="css/animate-custom.css" rel="stylesheet" />
    <link href="css/loginbox.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <?php include'navbar.php' ?>
   
  
    
    <div class="container">
        <div class="col-xs-1"></div>
 <div class="col-xs-9 box_shadow">

   <div class="page-header">
        <h3>Advance Search</h3>
      </div>
      <ul class="adv-search profile-details">
            <li><label for="booktitle">
                Book Title:
            </label> <input type="text" class="form-control"  id="booktitle" placeholder="Enter Book Title">
            </li>
            <li><label for="genre">
                Book Genre:
            </label> <input type="text" class="form-control"  id="authorname" placeholder="Enter book Genre">
            </li>
            <li><label for="Authorname">
                Author Name:
            </label> <input type="text" class="form-control"  id="authorname" placeholder="Enter Author Name">
            </li>
            <li><label for="pulisher">
               Publisher:
            </label> <input type="text" class="form-control"  id="publisher" placeholder="Enter Publisher">
            </li>
            <li><label for="edition">
                Book edition:
            </label> <input type="text" class="form-control"  id="edition" placeholder="Enter Book edition">
            </li>
            <li>
                <br>
                <input type="submit" class="btn btn-primary btn-block" value=" Click Here For Advance Search">
            </li>
            <br>
    </ul>

      
                            </div>

    </div> <!-- /container -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script> 
    <script src="js/global.min.js"></script>
    <script src="js/dropzone.js"></script>
    <script src="js/jqueryfxns.js"></script>
    <script type="text/javascript" src="js/jquery.dotdotdot.min.js"></script>
   
    
  </body>
</html>
