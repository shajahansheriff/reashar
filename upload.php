<?php

    require_once("worker/cake/models/config.php");  
    if(!isUserLoggedIn()) { header("Location: index.php"); die(); }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
     <title>Upload Book</title>
    <?php require_once 'web_css.php';?>
  </head>

  <body>

    <?php include'navbar.php' ?>
   
  
    
    <div class="container">
 
   <div class="col-xs-2">
        
    </div>


   	   <div class="col-xs-8 ">

    <?php echo '<div id="lu" style="display:none">';if(isset($loggedInUser->user_id)) echo $loggedInUser->user_id; echo '</div>';?>
         <div class="panel panel-default upload-div" id="main_details" >
              <div class="panel-heading"><h4>Enter Book Details</h4></div>
                  <div class="panel-body">
                      <div class="media"><!-- Needed For Uploading books -->
                  
                            <div class="media-body" >
                               
                                  <!-- <div class = "form-control">Gender: <div class = "radio-inline"><input type = "radio" name ="sex" value = "Male" checked>Male</input></div>&nbsp;<div class = "radio-inline"><input type = "radio" name ="sex" value = "Female">Female</input></div></div>   -->
                                  <!-- <div class = "form-control"  style = "height:75px;">Date of Birth: <div class = "input-sm" id ="daob"><input type = "date" id = "dob" name ="dob" style="height:20px"></div></div>   -->
                                  
                                              
                                               
                                    <div class="col-xs-12">          
                                              <select class="form-control" id="category">
                                                 <option value= -1>-- *Select a Category --</option>
                                                <option value = "science fiction">Science Fiction</option>
                                                <option value = "mythology">Mythology</option>
                                                <option value = "novel">Novel</option>
                                                <option value = "others">Others</option>
                                            </select>
                                  <p id = "errordivcategory" style = "color:red;"></p><br>                                    
                                  <input type = "text" class="form-control" placeholder = "*Title" id = "title" name ="title" >
                                  <small id = "errordivtitle" style="color:red" ></small>  <br>
                                  <input type = "text" class="form-control" placeholder = "*Author" id = "author" name ="author" >
                                  <small id = "errordivauthor" style = "color:red"></small><br> 
                                  <input type = "text" class="form-control" placeholder = "Publisher" id = "publisher" name ="publisher" >
                                  <small id = "errordivpublisher" style = "color:red"></small><br> 
                                  </div>
                                  <div class="col-xs-6"><input type = "text" class="form-control" placeholder = "edition" id = "edition" name ="Edition"></div>
                                   <div class="col-xs-6"><input type = "text" class="form-control" placeholder = "Print (yyyy)" id = "print" name ="print" ></div>
                                  <small id = "errordivedition" style="width:35%;float:left;color:red;"></small><small id = "errordivtitle" tyle="width:35%; float:right;color:red;"> </small>

                                  <!-- <div class = "form-control"  style = "height:75px;"><div class = "input-sm"></div></div> -->  
                                

                            </div>
                            <br>
                            <div class="col-xs-12"> 
                            <button id = "submitbookdetails" class="pull-right btn btn-primary">Submit</button>
                                <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="">Yes, This Book is written by me
                                                </label>
                                            </div>
                                </div>

                            
                  </div>
                  </div>
          </div>                     
                    
          <center>
          <div class="panel panel-default uplaod-div" id="uploadbook" style="display:none;" >
              <div class="panel-heading" data-toggle="modal" data-target="#myModal"><h4>Upload Book</h4></div>
                  <div class="panel-body">
                      <div class="media">
                                <div class="media-body" id = "getfile" style="width:150px;height:150px;">
                                <link href="css/dropzone.css" type="text/css" rel="stylesheet" />
                                
                                <center><form  class="dropzone"></form></center>
                                
                                </div>
                                <div id = "back1" style="margin:10px;"><a class="pull-right badge">Back</a></div>
                                <!-- <div id = "skip2" style="margin:10px;margin-right:68px;display:none;"><a class="pull-right badge">Skip</a></div> -->
                      </div>
                  </div>
          </div> 
          </center>   <div  class="dropzone_type" value = "book"></div>
          <center>
          <div class="panel panel-default uplaod-div" id="uploadcover" style="display:none;">
              <div class="panel-heading"><h4>Upload Cover Photo for your Book</h4></div>
                  <div class="panel-body">
                      <div class="media">
                                <div class="media-body" id = "getfile" style="width:150px;height:150px;">
                                
                                <center><form class="dropzone"></form></center>
                                
                                </div>
                                <div id = "cancel_cover" style="margin:10px;"><a class="pull-right badge">No Need</a></div>
                                <!-- <div id = "skip2" style="margin:10px;margin-right:68px;display:none;"><a class="pull-right badge">Skip</a></div> -->
                      </div>
                  </div>
          </div> 
          <div class="panel panel-default uplaod-div" id="upload_complete"  style="height:300px;padding-bottom:10px;display:none" id="upload_complete">
              <div class="panel-heading"><h4>Upload Complete!</h4></div>
                  <div class="panel-body" style="height: 100%">
                      <div class="media" style="height: 100%">
                                <div class="media-body" style="width:150px;height:80%;">
                                
                             
                                <div>
                                <div ><a href="#" name = "uploaded_book_url"><img id="uploaded_book_image" src="" style = "width:110px; height:150px;" class="img-panel"></a></div>
                                <div style = "inline-block" id="uploaded_book_title" ></div>
                                <div class="panel-footer"id = "uploaded_book_category"><small></small></div>  </div>
                                </div>
                                <p>
                                    <a class="btn btn-xs btn-default" id = "uploaded_book_read" name = "uploaded_book_url" href="#">Read Now</a>
                                    <a href="#"class="btn btn-xs btn-default" data-toggle="modal" data-target="#uploadconfirm">Not Now</a>
                              </p>
                   
                    </div>
                  </div>
          </div> 
          </center>   
          </div>



<!-- modal-start -->
<div class="modal fade" id="uploadconfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button class="btn pull-right glyphicon glyphicon-remove close" data-dismiss="modal"></button>
        <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
      </div>
      <div class="modal-body ">

        want to upload more books? <a href="upload.php" class="btn btn-primary btn-sm">Yes</a> or <a href="profile.php<?php echo '?id='.$loggedInUser->user_id;?>" class="btn btn-primary btn-sm">No</a>
        
      </div>
      <div class="modal-footer ">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- modal-end -->

    </div> <!-- /container -->



    <?php require_once'web_js.php'; ?>
    
  </body>
</html>
