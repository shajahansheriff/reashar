    <?php

    require_once("worker/cake/models/config.php");  
    if(!isUserLoggedIn()) { header("Location: index.php"); die(); }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Note there is no responsive meta tag here -->

    <link rel="shortcut icon" href="img/favicon.png">

    <title>Welcome | Reashar</title>

    <!-- Bootstrap core CSS -->
    <link href="css/global.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/non-responsive.css" rel="stylesheet">
    <link href="css/animate-custom.css" rel="stylesheet" />
    <link href="css/loginbox.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <?php include'navbar.php' ?>
   
  
    
    <div class="container">
        <div class="col-xs-3"></div>
 <div class="col-xs-6 box_shadow">

   <div class="page-header page-header-cust">
        <button id="edit" class="pull-right btn btn-sm btn-success"><i class="fa fa-gear fa-fw"></i> Edit</button><h3>Personal Settings</h3>
      </div>
      <ul class="adv-search profile-details">
            <li>
              
              <label for="username">
                User Name: 
            </label> <input type="text" class="form-control " disabled="disabled" id="username" value="Shajahan.Sheriff">
            <div id="confirmation" style="display:none;"><label>Password</label>
            <input type="password" class="form-control" placeholder="Password">
            </div>
            </li>
            <li><label for="email">
               Email:
            </label> <input type="email" class="form-control" disabled id="email" value="Shajansheriff@gmail.com">
            </li>
            <li><label for="password">
                Password:
            </label> <input type="password" class="form-control" disabled id="password" value="******">
            </li>
            <li><label for="confirm_password">
                Confirm Password:
            </label> <input type="password" class="form-control" disabled id="confirm_password" value="******">
            </li>
             <li>
                <br>
                <input type="submit" class="btn btn-primary btn-block" value=" Click to Submit">
            </li>
            <br>
    </ul>

      
                            </div>

    </div> <!-- /container -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script> 
    <script src="js/global.min.js"></script>
    <script src="js/dropzone.js"></script>
    <script src="js/jqueryfxns.js"></script>
    <script type="text/javascript" src="js/jquery.dotdotdot.min.js"></script>
   <script type="text/javascript">
    $('#edit').click(function(){ // click to
            $('#username').attr('disabled',false); // removing disabled in this class
 });
   </script>
    
  </body>
</html>
