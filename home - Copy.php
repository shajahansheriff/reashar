<?php

    require_once("worker/cake/models/config.php");  
    if(!isUserLoggedIn()) { header("Location: index.php"); die(); }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Note there is no responsive meta tag here -->

    <link rel="shortcut icon" href="img/favicon.png">

    <title>Welcome | Reashar</title>

    <!-- Bootstrap core CSS -->
    <link href="css/global.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/non-responsive.css" rel="stylesheet">
    <link href="css/animate-custom.css" rel="stylesheet" />
    <link href="css/loginbox.css" rel="stylesheet" />
    <link href="css/style1.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <?php include'navbar.php' ?>
   
  
    
    <div class="container">
 
   <div class="col-xs-3">
        <div class="panel panel-default">
          <div class="panel-heading"><a href="#" class="pull-right" style="font-size: 12px">View all (4)</a> <h4>Current Reading</h4></div>
   			   <div class="panel-body">
              	<div class="list-group">
                <a href="#" class="list-group-item" id="book_title" value = "100">Harry Potter and The Oder of phoenix qwerty <span class="badge">540</span></a>
                <a href="#" class="list-group-item">Two States <span class="badge">120</span></a>
                <a href="#" class="list-group-item">Three Mistakes<span class="badge">230</span></a>
              	</div>
            </div>
   	    </div>
        <div class="panel panel-default">
          <div class="panel-heading"><a href="#" class="pull-right" style="font-size: 12px">View all (4)</a> <h4>Work in Progress</h4></div>
           <div class="panel-body">
                <div class="list-group">
                <a href="#" class="list-group-item">Harry Potter and The Oder of phoenix qwerty <span class="badge">540</span></a>
                <a href="#" class="list-group-item">Two States <span class="badge">120</span></a>
                <a href="#" class="list-group-item">Three Mistakes<span class="badge">230</span></a>
                </div>
            </div>
        </div>
    </div>

<?php include 'stream.php'; ?>
   	    <!-- 3 column -->

        <div class="col-xs-3">
           
          
            <div class="media "> 
               <div class="panel-heading"><a href="#"  ><h4><b class="glyphicon glyphicon-edit pull-left" style="padding-right:7px;"></b>Write Books</h4></a></div>
               <div class="panel-heading"><a href="#"  ><h4><b class="glyphicon glyphicon-upload pull-left" style="padding-right:7px;"></b>Upload Books</h4></a></div>
               <div class="panel-heading"><a href="#"  ><h4><b class="glyphicon glyphicon-search pull-left" style="padding-right:7px;"></b>Advance Search</h4></a></div>
            </div>
            
        <div class="panel panel-default">
          <div class="panel-heading"><a href="#" class="pull-right view-all" >View all</a> <h4>Suggestions</h4></div>
   			     <div class="panel-body">
 
              <div class="media" id="as1">
                   
                    <a class="pull-left" href="#">
                      <img class="media-object img-circle" src="photos/avatar.jpg" >
                    </a>
                    <div class="media-body" >
                  <a href="#" > <i class="glyphicon glyphicon-remove pull-right close" id="1"></i>  	</a>
                      <h5 class="media-heading"> <a href="#" >Shajahan Sheriff </a> </h5> 
                      	
                         <p>
                            <a href="#" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-heart"></span> Connect</a>
                            <a href="#" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-comment"></span> Message</a>
                         </p>

                    </div>

             </div>
                          
             <div class="media" id="as2">
                   
                    <a class="pull-left" href="#">
                      <img class="media-object img-circle" src="photos/avatar.jpg" >
                    </a>
                    <div class="media-body">
                  <a href="#" id=""> <i class="glyphicon glyphicon-remove pull-right close" id="2"></i>  	</a>
                      <h5 class="media-heading"> <a href="#" >Shajahan AKbar Sheriff qwerty</a> </h5> 
                      	
                         <p>
                            <a href="#" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-heart"></span> Connect</a>
                            <a href="#" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-comment"></span> Message</a>
                         </p>

                    </div>

             </div>
   	    </div>
    </div>

    <?php echo '<div id="lu" style="display:none">';if(isset($loggedInUser->user_id)) echo $loggedInUser->user_id; echo '</div>';?>
         <div class="panel panel-default" id="main_details" style = "display:none;">
              <div class="panel-heading"><h4>Enter Book Details</h4></div><hr>
                  <div class="panel-body">
                      <div class="media" id = "form"><!-- Needed For Uploading books -->
                  
                            <div class="media-body" >
                                <center>
                                  <!-- <div class = "form-control">Gender: <div class = "radio-inline"><input type = "radio" name ="sex" value = "Male" checked>Male</input></div>&nbsp;<div class = "radio-inline"><input type = "radio" name ="sex" value = "Female">Female</input></div></div>   -->
                                  <!-- <div class = "form-control"  style = "height:75px;">Date of Birth: <div class = "input-sm" id ="daob"><input type = "date" id = "dob" name ="dob" style="height:20px"></div></div>   -->
                                  <div class = "form-control"  style = "height:75px;" id = "categorydiv"><div class = "input-sm">
                                              <select id="category" style="width:80%;text-align:center;">
                                                <option value= -1>-- Select a Category --</option>
                                                <option value = "science fiction">Science Fiction</option>
                                                <option value = "mythology">Mythology</option>
                                                <option value = "novel">Novel</option>
                                                <option value = "others">Others</option>
                                              </select>
                                  </div><small id = "errordivcategory" style = "color:red"></small></div>                                    
                                  <div class = "form-control"  style = "height:75px;" id = "titlediv"><div class = "input-sm"><input type = "text" placeholder = "Title" id = "title" name ="title" style="width:80%; text-align:center;"></div><small id = "errordivtitle" style = "color:red"></small></div>  
                                  <div class = "form-control"  style = "height:75px;" id = "authordiv"><div class = "input-sm"><input type = "text" placeholder = "Author" id = "author" name ="author" style="width:80%; text-align:center;"></div><small id = "errordivauthor" style = "color:red"></small></div>  
                                  <div class = "form-control"  style = "height:75px;" id = "publisherdiv"><div class = "input-sm"><input type = "text" placeholder = "Publisher" id = "publisher" name ="publisher" style="width:80%; text-align:center;"></div><small id = "errordivpublisher" style = "color:red"></small></div>  
                                  <div class = "form-control"  style = "height:75px;" id = "otherdiv"><div class = "input-sm"><input type = "text" placeholder = "edition" id = "edition" name ="Edition" style="width:35%;"><input type = "text" placeholder = "Print (yyyy)" id = "print" name ="print" style="width:35%;margin-left:10%;"></div><small id = "errordivedition" style="width:35%;float:left;color:red;"></small><small id = "errordivtitle" tyle="width:35%; float:right;color:red;"> </small></div>  
                                  <!-- <div class = "form-control"  style = "height:75px;"><div class = "input-sm"></div></div> -->  
                                </center>
                            </div>
                            <div id = "cancel" style="margin:10px;"><a class="pull-right badge">Cancel</a></div>
                            <div id = "submitbookdetails" style="margin:10px;margin-right:68px;"><a class="pull-right badge">Submit</a></div>
                  </div>
                  </div>
          </div>                     
                    
          <center>
          <div class="panel panel-default" id="uploadbook" style="display:none;">
              <div class="panel-heading"><h4>Upload Book</h4></div><hr>
                  <div class="panel-body">
                      <div class="media" id = "form">
                                <div class="media-body" id = "getfile" style="width:150px;height:150px;">
                                <link href="css/dropzone.css" type="text/css" rel="stylesheet" />
                                
                                <center><form  class="dropzone"></form></center>
                                
                                </div>
                                <div id = "back1" style="margin:10px;display:none;"><a class="pull-right badge">Back</a></div>
                                <!-- <div id = "skip2" style="margin:10px;margin-right:68px;display:none;"><a class="pull-right badge">Skip</a></div> -->
                      </div>
                  </div>
          </div> 
          </center>   <div  class="dropzone_type" value = "book"></div>
          <center>
          <div class="panel panel-default" id="uploadcover" style="display:none;">
              <div class="panel-heading"><h4>Upload Cover Photo for your Book</h4></div><hr>
                  <div class="panel-body">
                      <div class="media" id = "form">
                                <div class="media-body" id = "getfile" style="width:150px;height:150px;">
                                <link href="css/dropzone.css" type="text/css" rel="stylesheet" />
                                
                                <center><form class="dropzone"></form></center>
                                
                                </div>
                                <div id = "back1" style="margin:10px;display:none;"><a class="pull-right badge">Back</a></div>
                                <!-- <div id = "skip2" style="margin:10px;margin-right:68px;display:none;"><a class="pull-right badge">Skip</a></div> -->
                      </div>
                  </div>
          </div> 
          </center>   





    </div> <!-- /container -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script> 
    <script src="js/global.min.js"></script>
    <script src="js/dropzone.js"></script>
    <script src="js/jqueryfxns.js"></script>
    <script type="text/javascript">
$(function ()
{ $(".close").click(function(){
var element = $(this);
var I = element.attr("id")
$('#as'+I).hide();
return false;
});
});
    </script>
    
  </body>
</html>
