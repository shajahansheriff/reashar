
<!-- Fixed navbar --> 
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container nav-container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="index.php" style="padding-left:10px;"><img src="img/logo.png" class="logo"></a>
        </div>
     <?php if(isset($loggedInUser->user_id)){
      echo '
     <div class="navbar-collapse collapse" >
     	<ul class="nav navbar-nav" style="margin-left:15px; ">
            
            <li><a href="comingsoon.php"><span class="glyphicon glyphicon-flag   nav-icon"></span>Activity</a></li>
            <li><a href="upload.php" id="upload"><span class="fa fa-cloud-upload   nav-icon"></span>Upload</a></li>
            
          </ul>
          <div class="form col-xs-5" style="padding-top: 8px; width:41%;"> 
          	<a onclick="search();" class="glyphicon glyphicon-search pull-right search-btn" ></a>
          	<form action="index.php">
              <input type="text" id="search"class="form-control "placeholder="Search..."  style="padding-right: 34px;"/>
            </form>
          	
       
          <div class="list-group search-result">

          </div> 
        	    
          </div>
        
          <ul class="nav navbar-nav">
            
            
           <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope"></i>
                                <span class="label label-danger">4</span>
                            </a>
                            <ul class="dropdown-menu">
                                 <li class="header"><span class="pull-right"><a href="#" >Compose New</a></span>Messages</li>
                                 
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <li><!-- start message -->
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="photos/a.jpg" class="img-circle" alt="User Image"/>
                                                </div>
                                                
                                                <h4>
                                                    Support Team
                                                    <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                </h4>

                                                <p>Why not buy a new awesome theme?Why not buy a new awesome theme?</p>

                                            </a>
                                        </li><!-- end message -->
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="img/avatar2.png" class="img-circle" alt="user image"/>
                                                </div>
                                                <h4>
                                                    AdminLTE Design Team
                                                    <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="img/avatar.png" class="img-circle" alt="user image"/>
                                                </div>
                                                <h4>
                                                    Developers
                                                    <small><i class="fa fa-clock-o"></i> Today</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="img/avatar2.png" class="img-circle" alt="user image"/>
                                                </div>
                                                <h4>
                                                    Sales Department
                                                    <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="img/avatar.png" class="img-circle" alt="user image"/>
                                                </div>
                                                <h4>
                                                    Reviewers
                                                    <small><i class="fa fa-clock-o"></i> 2 days</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">See All Messages</a></li>
                            </ul>
                        </li>
                        <!-- Notifications-->

           <li class="dropdown messages-menu">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-flash"></i>
                                <span class="label label-danger">10</span>
                            </a>
                            <ul class="dropdown-menu">
                               
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <li><!-- start message -->
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="photos/a.jpg" class="img-circle" alt="User Image"/>
                                                </div>
                                                <div class="pull-right nav-project">
                                                    <img src="photos/a.jpg" class="img-thumbnail" alt="User Image"/>
                                                    
                                                </div>
                                                <h4>
                                                    Support Team  Support Team
                                                   
                                                </h4>

                                                <p>Liked Your Book</p>

                                            </a>
                                        </li><!-- end message -->
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="img/avatar2.png" class="img-circle" alt="user image"/>
                                                </div>
                                                <h4>
                                                    AdminLTE Design Team
                                                    <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="img/avatar.png" class="img-circle" alt="user image"/>
                                                </div>
                                                <h4>
                                                    Developers
                                                    <small><i class="fa fa-clock-o"></i> Today</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="img/avatar2.png" class="img-circle" alt="user image"/>
                                                </div>
                                                <h4>
                                                    Sales Department
                                                    <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="img/avatar.png" class="img-circle" alt="user image"/>
                                                </div>
                                                <h4>
                                                    Reviewers
                                                    <small><i class="fa fa-clock-o"></i> 2 days</small>
                                                </h4>
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">See All Messages</a></li>
                            </ul>
                        </li>
                        <!-- Tasks: style can be found in dropdown.less -->
                        <li><a href="#main_details" id="upload"><span class="fa fa-home nav-icon"></span>Home</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user nav-icon"></span>'. $loggedInUser->fname.'</a>
              <ul class="dropdown-menu">
                <li><a href="profile1.php?id='.$loggedInUser->user_id.'"><b class="glyphicon glyphicon-user nav-icon-li"></b>Profile</a></li>
                <li><a href="#"><b class="glyphicon glyphicon-th nav-icon-li"></b>My Shelf</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                
                <li><a href="#"><span class="glyphicon glyphicon-cog nav-icon-li"></span>Account Settings</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-log-out nav-icon-li"></span>Logout</a></li>
              </ul>
            </li>
          </ul>
          
        </div>  <!--/.nav-collapse -->
        ';
        } ?>
      </div>
    </div>

    
