<?php

    require_once("worker/cake/models/config.php");  
    if(!isUserLoggedIn()) { header("Location: index.php"); die(); }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="Reashar - A social Neork ">
    <meta name="author" content="Web_Dev - Prathap || Web_Designer - Shajahan">

    

    
    <?php require_once 'web_css.php'; ?>
    <title>Welcome | Reashar</title>

    
  </head>

  <body>

    <?php include'navbar.php' ?>
   
  
    
    <div class="container">
 
   <div class="col-xs-3">
        <div class="panel panel-default">
          <div class="panel-heading"><a href="#" class="pull-right" style="font-size: 12px">View all (4)</a> <h4>Current Reading</h4></div>
   			   <div class="panel-body">
              	<div class="list-group">
                <a href="#" class="list-group-item" id="book_title" value = "100">Harry Potter and The Oder of phoenix qwerty <span class="badge">540</span></a>
                <a href="#" class="list-group-item">Two States <span class="badge">120</span></a>
                <a href="#" class="list-group-item">Three Mistakes<span class="badge">230</span></a>
              	</div>
            </div>
   	    </div>
        <div class="panel panel-default">
          <div class="panel-heading"><a href="#" class="pull-right" style="font-size: 12px">View all (4)</a> <h4>Work in Progress</h4></div>
           <div class="panel-body">
                <div class="list-group">
                <a href="#" class="list-group-item">Harry Potter and The Oder of phoenix qwerty <span class="badge">540</span></a>
                <a href="#" class="list-group-item">Two States <span class="badge">120</span></a>
                <a href="#" class="list-group-item">Three Mistakes<span class="badge">230</span></a>
                </div>
            </div>
        </div>
    </div>

<?php include 'stream.php'; ?>
   	    <!-- 3 column -->

        <div class="col-xs-3">
           
          
            <div class="media "> 
               <div class="panel-heading"><a href="comingsoon.php"  data-toggle="modal" data-target="#myModal"><h4><b class="glyphicon glyphicon-edit pull-left" style="padding-right:7px;" ></b>Write Books</h4></a></div>
               <div class="panel-heading"><a href="upload.php"  ><h4><b class="glyphicon glyphicon-upload pull-left" style="padding-right:7px;"></b>Upload Books</h4></a></div>
               <div class="panel-heading"><a href="comingsoon.php"  ><h4><b class="glyphicon glyphicon-search pull-left" style="padding-right:7px;"></b>Advance Search</h4></a></div>
            </div>
            
        <div class="panel panel-default">
          <div class="panel-heading"><a href="#" class="pull-right view-all" >View all</a> <h4>Suggestions</h4></div>
   			     <div class="panel-body" id = "user_suggestions" style="overflow:hidden;height:125px" value= "10">



          <?php
             $reply =  get_user_suggestions($loggedInUser->user_id);   // funcs.php
             if($reply=="") $reply = get_user_suggestions($loggedInUser->user_id,4);//CAll  wiht new limit if no results        
             echo $reply
          ?>

             </div>
   	    </div>
    </div>

    
          <!-- Modal -->
<div class="modal fade" id="myModal" value ="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" id ="mod-content">
      
    </div>
  </div>
</div>
 <!-- modal-end -->





    </div> <!-- /container -->



    <?php require_once 'web_js.php'?>
   
    
  </body>
</html>
