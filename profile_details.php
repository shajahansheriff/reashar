<?php
   
    require_once("worker/cake/models/config.php");
    if(!isUserLoggedIn()) { header("Location: index.php"); die(); }

    //$username = $loggedInUser->username;
    $user_id = (int)$_GET['id']; 
    $userdetails = fetchUserDetailsById($user_id);
    //foreach ($userdetails as $key => $value) {
    // echo $key."=>".$value;
   //}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Note there is no responsive meta tag here -->

    <link rel="shortcut icon" href="img/favicon.png">

    <title>Welcome | Reashar</title>

    <!-- Bootstrap core CSS -->
    <link href="css/global.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/non-responsive.css" rel="stylesheet">
    <link href="css/animate-custom.css" rel="stylesheet" />
    <link href="css/loginbox.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
     <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <?php 
    include'navbar.php' ?>
    <?php echo '<div id ="lu" style ="display:none;">'.$loggedInUser->user_id.'</div>'; ?>
   
  
    
    <div class="container">
 
   <div class="col-xs-3" >
        <div class="panel panel-default">
          <div class="panel-heading"><a href="#" class="pull-right" style="font-size: 12px" id = "edit_profile"> edit</a> <h4>Profile</h4></div>
   			   <div class="panel-body profile-body">
              
              	<div >
                  <img id="profile_pic" src="<?php if(isset($userdetails['profile_pic'])) echo $userdetails['profile_pic']; else echo "photos/avatar.jpg";?> " class="profile-pic">
                  </div>
                   <p style="text-align:center">
                            <a href="#" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-heart"></span> Follow</a>
                            <a href="#" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-comment"></span> Message</a>
                         </p>
               <ul class="profile-ul">
                <li><b><?php echo $userdetails['fname']."&nbsp;".$userdetails['lname'];?></b></li>
                <li><b>Job: </b><?php if(isset($userdetails['occupation'])) echo " ".$userdetails['occupation']; else echo " None";?></li>
                <li><b>Lives:</b><span style="display:inline-block"><?php if(isset($userdetails['location'])) echo " ".$userdetails['location']; else echo " None";?></span></li>
                <li><a href="#">See more</a></li>
              </ul>
        
              
    
                	
            </div>

   	    </div>
        <div class="list-group">
                <a id = "followers" class = "list-group-item"  href="#"><b>Followers</b><span class="badge" id="follower_count"><?php echo $userdetails['followercount']; ?> </span></a>
                <a id = "followings" class = "list-group-item"  href="#"><b>Following</b><span class="badge" id="following_count"><?php echo $userdetails['followingcount']; ?> </span></a> 
                <a id = "followers" class = "list-group-item"  href="#"><b>Shelf  </b></a>
                <a href="#" class="list-group-item">Current Reading <span class="badge">120</span></a>
<div id="lu" class = "hidden_item"><?php if(isset($loggedInUser->user_id)) echo $loggedInUser->user_id; echo '';?></div>
    
              </div>  
        </div>
 
  <div class="col-xs-4">
<div class="panel panel-default box_shadow">
                        <div class="panel-heading">
                            <i class="fa fa-user"></i> General Information
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <ul class="profile-ul profile-details">
    <li><span> Name:</span> Shajahan</li>
    <li><span><i class="fa fa-calendar "></i> Data of Birth:</span> 04-May-1995</li>
    <li><span><i class="fa fa-male"></i> Sex:</span> Male</li>
    <li><span><i class="fa fa-map-marker"></i> Lives in:</span> Chennai</li>
    
    </ul>

</div>
  </div>
</div>
 <div class="col-xs-5">
<div class="panel panel-default box_shadow">
                        <div class="panel-heading">
                            <i class="fa fa-list-alt"></i> Contact Details
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <ul class="profile-ul profile-details">
    <li><span><i class="fa fa-mobile"></i> Mobile:</span> 91-8122266535</li>
    <li><span><i class="fa fa-phone-square "></i> Office:</span> 044-2345678</li>
    <li><span><i class="fa fa-envelope-o"></i> Mail: </span> shajansheriff@gmail.com</li>
    <li><span><i class="fa fa-chain"></i> Website:</span> www.killerbee.in</li>
    
    </ul>

</div>
  </div>
</div>
<hr>
  <div class="col-xs-5 mrg-top">
<div class="panel panel-default box_shadow">
                        <div class="panel-heading">
                            <i class="fa fa-gears"></i> Work
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <ul class="profile-ul profile-details">
    <li><span><i class="fa fa-suitcase"></i> Job:</span> UI Designer</li>
    <li><span><i class="fa fa-wrench "></i> Skills:</span> Photoshop | Illustrator | CSS3</li>
    
    </ul>

</div>
  </div>
</div>
<div class="col-xs-4 mrg-top">
<div class="panel panel-default box_shadow">
                        <div class="panel-heading">
                            <i class="fa fa-book"></i> Education
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <ul class="profile-ul profile-details">
    <li><span> High School:</span> Sri Ramakrishna Matric Hr sec School</li>
    <li><span> College:</span> College Of Engineering, Guindy</li>
    
    </ul>

</div>
  </div>
</div>
<div class="col-xs-9 mrg-top">
<div class="panel panel-default box_shadow">
                        <div class="panel-heading">
                            <i class="fa fa-info-circle"></i> Further Details
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <ul class="profile-ul profile-details">
                          <li><span><i class="fa fa-bullhorn"></i> Language: </span> Tamil | English</li>
    <li><span><i class="fa fa-bullseye"></i> Interests:</span> Coding | Designing</li>
    <li><span><i class="fa fa-magic "></i> Hobby:</span> Video Games | Movies</li>
    <li><span><i class="fa fa-smile-o"></i> About me: </span> A Gentleman with humour</li>
    <li><span><i class="fa fa-star"></i> Quote:</span> Hell Fuck Hard</li>
    
    </ul>

</div>
  </div>
</div>
          

   
    

    <div class="col-xs-8" id = "edit_profile_div" style="display:none;width:100%">
	         <div class="panel panel-default">
              <div class="panel-heading"><a href="#" class="pull-right" style="font-size: 12px" id = "close_profile_div">Close</a> <h4>Edit Profile</h4></div>
              <div class="panel-body">
                    <?php include 'profile_div.php'; ?>
              </div> <!-- Panel body -->
            </div> <!-- Panel -->
   	</div>
    <div class="col-xs-8" id = "edit_profile_pic_div" style="display:none;width:100%">
            <div class="panel panel-default" id = "profile_pic_panel">
             <div class="panel-heading"><a href="#" class="pull-right" style="font-size: 12px" id = "close_profile_pic_div">Close</a> <h4>Edit Profile Picture</h4></div>
             <div class="panel-body">


             
                  <link href="css/dropzone.css" type="text/css" rel="stylesheet" />
                  <div  class="dropzone_type" value = "profile_picture"></div>
                  <center><form class="dropzone" ></form></center>
     
            </div> <!-- Panel body -->
          </div> <!-- Panel -->
    </div> <!-- /container -->
   



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/dropzone.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <script src="js/global.min.js"></script>
    <script src="js/jqueryfxns.js"></script>
    <script src="js/jquery.geocomplete.js"></script>
    <script type="text/javascript">
      $(function() {
        $('#asd').click(function() {
        $(".media-body").toggle(20);
        });
      });
      $(function(){
        $("#location").geocomplete()
      });
    </script>
    
    
    
   
    
    
  </body>
</html>
