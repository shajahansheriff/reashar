/** The following functions are for the profile page */

/* To get Profile Details */
$("#edit_profile").click(function(e){
    $("#edit_profile_div").css("display","block");
});
$("#edit_profile_pic").click(function(e){
    $("#edit_profile_pic_div").css("display","block");
});

$(".detail_present").click(function (e){
      var vax = $(this).html();
      $(this).css("display","none");
      var input = $(this).siblings(".no_details");
        $(input).css("display","block");
        if($(input).find(".profile_entered_value").attr("name") != "sex")
        {
          $(input).find(".profile_entered_value").attr("value",vax);
           
        }$(input).find(".profile_entered_value").focus();
});
    
$(".profile_entered_value").on("change",function(e){
          var key = ($(this).attr("type") == "radio") ? "sex" : $(this).attr("id");
           value =  $(this).val();
          if(key == "dob_day" || key == "dob_month" || key == "dob_year")
          {
            var day = $("#dob_day").val();
              var month = $("#dob_month").val();
              var year = $("#dob_year").val();
              if( day != -1 &&  month != -1 && year != -1 )
                {
                   value = year +"-"+ month +"-"+ day;  
                   key = "dob";
                }
              else
                return;
              }

          var functionname = "edit_profile";
          $.post("worker/callfunctions.php",{functionname:functionname,key:key,value:value},function(datax)
            {});
          if(($(this).attr("type") == "radio")){ 
                  var divi = $(".profile_entered_value_div");
                  //divi.css("display","none");
                  var parent = divi.parent();
          }
          else
            var parent = $(this).parent();
           $(parent).css("display","none");
           var detail = $(parent).siblings(".detail_present");
             $(detail).html(value);
             $(detail).css("display","block");
          
});

$(".profile_entered_value").on("keyup",function(e){
          if(e.keyCode ==27){     
                  if(($(this).attr("type") == "radio")){
                       var divi = $(".profile_entered_value_div");
                       //divi.css("display","none");
                       var parent = divi.parent();
                   }
                   else
                       var parent = $(this).parent();
                   $(parent).css("display","none");
                   var detail = $(parent).siblings(".detail_present");
                   $(detail).css("display","block");
          }
          
});
$(".profile_entered_value").on("focusout",function(e){
      
          if(($(this).attr("type") != "radio") && $(this).prop("tagName") != "SELECT"){
                    var parent = $(this).parent();
                    $(parent).css("display","none");
                    var detail = $(parent).siblings(".detail_present");
                    $(detail).css("display","block");
 
          }
});

/*
$('[name="close"]').click(function(e){
          $($($(this)).parent()).parent().css("display","none");
});*/

$("#close_profile_div").on("click",function(){
        $("#edit_profile_div").css('display','none');
})
$("#close_profile_pic_div").on("click",function(){
        $("#edit_profile_pic_div").css('display','none');
})



// Functions for getting profile_details ends.


// Functions for profile page ends.








/** The following functions are for the home page */

/* The following functions are for upload book module */


  
$("#upload").click(function(e){
        $("#main_details").css('display','block');
        $("#submitbookdetails").css('display','block');
        $("#cancel").css('display','block');
        $("#uploadbook").css('display','none');
        $("#uploadcover").css('display','none');
        $("#upload_complete").css('display','none');
});

$('#category').click(function(e)
   {
    if($('#errordivcategory').html()) $('#errordivcategory').html("");
   });
   $('#title').keydown(function(e)
   {
    if($('#errordivtitle').html()) $('#errordivtitle').html("");
   });
   $('#publisher').keydown(function(e)
   {
    if($('#errordivpublisher').html()) $('#errordivpublisher').html("");
   });
   $('#author').keydown(function(e)
   {
    if($('#errordivauthor').html()) $('#errordivauthor').html("");
   });
   $('#edition').keydown(function(e)
   {
    if($('#errordivedition').html()) $('#errordivedition').html("");
   });
    $('#print').keydown(function(e)
   {
    if($('#errordivprint').html()) $('#errordivprint').html("");
   });

  // Field Validation
   $('#submitbookdetails').click(function(e)
    {
        e.preventDefault();
        var functionname = "insertBookDetails";//DOB,Sex,Occupation,Institution,Location
        var category = $('#category').val();
        if(category == -1 ) {if($('#errordivcategory').html() != "") return 0; else {$('#errordivcategory').html('category field cannot be empty');return 0;}}
        var title = $('#title').val();
        if(title == "" || !title) {if($('#errordivtitle').html() != "") return 0; else {$('#errordivtitle').html('Title field cannot be empty');return 0;}}
        var author = $('#author').val();
        if(author == "" || !author) {if($('#errordivauthor').html() != "") return 0; else {$('#errordivauthor').html('Please Enter author of the book');return 0;}}
        
        var publisher = $('#publisher').val();
        if(publisher == "" || !publisher) {if($('#errordivpublisher').html() != "") return 0; else {$('#errordivpublisher').html('Please enter the publisher of the book');return 0;}}
        var edition = $('#edition').val();
        if(isNaN(edition) || edition < 0 || edition > 999) {if($('#errordivedition').html() != "") return 0; else {$('#errordivedition').html('Enter a valid edition of the book');return 0;}}
        var print = $('#print').val();
        if(isNaN(print) || print < 0 || print > 9999) {if($('#errordivprint').html() != "") return 0; else {$('#errordivprint').html('Enter a valid print of the book');return 0;}}
        var uploadedby = $('#lu').html();
        
              $("#main_details").css('display','none');
              $("#uploadbook").css('display','block');
              //$("#back1").css('display','block');
              
        }); 
        
    
    $('#cancel').click(function(e)
    {
       $("#main_details").css('display','none');
    });
 
    $('#back1').click(function(e) //Button at the upload Book 
    {
        e.preventDefault();
        $("#main_details").css('display','block');
        $("#uploadbook").css('display','none');
    });

  


   // To insert into Incomplete fields in the dB
   $('#submitvalues').click(function(e)
    {
        e.preventDefault();
        var functionname = "insertTextValues";//DOB,Sex,Occupation,Institution,Location
        var sex = $('input[name=sex]:checked').val();
        var dob = $('#dob').val();
        var occupation = $('#occupation').val();
        var institution = $('#institution').val();
        var city = $('#city').val();
        var state = $('#state').val();
        var country = $('#country').val();
           $.post("../callfunctions.php",{functionname:functionname,sex:sex,dob:dob,occupation:occupation,institution:institution,city:city,state:state,country:country},function(datax)
        {
          $("#city1").html(datax);
              $("#main_details").css('display','none');
              $("#submitvalues").css('display','none');
              $("#skip1").css('display','none');
              $("#profilepic").css('display','block');
              $("#back1").css('display','block');
              $("#skip2").css('display','block');
        }); 
        
    });

 $("#cancel_cover").on("click",function(e){
          var functionname = "getbd";
              $.post("worker/callfunctions.php",{functionname:functionname},function(datax)
              {
                    var datax = jQuery.parseJSON(datax);
                    $("#uploadcover").css('display','none'); 
                    $("#uploaded_book_image").attr('src',datax['cover']);
                    $("#uploaded_book_title").html(datax['title']);
                    $("#uploaded_book_category").html(datax['category']);
                    $("[name='uploaded_book_url']").attr('href',"viewer.php?link="+datax['link']);
                    $("#upload_complete").css('display','block');  
                    $(".dropzone_type").attr("value","book");
               });
});
 $("#uploaded_book_no_read, #uploaded_book_read").on("click",function(e){
    $("#upload_complete").css('display','none');
 });
/* Functions for upload book module ends...  */






    $('#skip1').click(function(e)
    {
        e.preventDefault();
        $("#main_details").css('display','none');
        $("#submitvalues").css('display','none');
        $("#skip1").css('display','none');
        $("#profilepic").css('display','block');
        $("#back1").css('display','block');
        $("#skip2").css('display','block');



    });
  

//LIKE the book
$(document).on("click", "[name = 'like']",function()
{
  var panel = $(this).closest(".panel");
  var bookid = panel.attr("value");
  if(!bookid)
   { var modal = $(this).closest("#myModal");
       var bookid = modal.attr("value");}

  $(this).addClass('text-primary');
  $(this).attr("name","unlike");
  var count = parseInt($($(this).parent()).find("span#count").html());
  var count = parseInt($($(this).parent()).find("span#count").html(count + 1));
  $.ajax({
                      url: "worker/callfunctions.php",
                      type: "POST",
                      cache: false,
                      data: {functionname:"add_like",bookid:bookid},
                      
                      success: function(datax){
                        
                        }
                      
                    });
});
$(document).on("click", "[name = 'unlike']",function(e)
{
  var panel = $(this).closest(".panel");
  var bookid = panel.attr("value");
  if(!bookid)
  {  var modal = $(this).closest("#myModal");
    var bookid = modal.attr("value");}
  var count = parseInt($($(this).parent()).find("span#count").html());
  var count = parseInt($($(this).parent()).find("span#count").html(count - 1));
  $(this).removeClass('text-primary');
  e.stopPropagation();
  $(this).attr("name","like");
  $.ajax({
                      url: "worker/callfunctions.php",
                      type: "POST",
                      cache: false,
                      data: {functionname:"remove_like",bookid:bookid},
                     
                      success: function(datax){
                        
                        }
                      
                    });
});






// Pass Value TO Modal Box

$(document).on("click", "#book_img, #book_title", function(e)
{
  var panel = $(this).closest(".panel");
  var bookid = panel.attr("value");

  $("#myModal").attr("value",bookid);
  $.ajax({
                      url: "worker/callfunctions.php",
                      type: "POST",
                      cache: false,
                      data: {functionname:"get_book_details",bookid:bookid},
                     
                      success: function(datax){
                        $("#mod-content").html(datax);
                        }
                      
                    });
})


$(document).on("click", "[name='messageto']", function(e)
{
  
  var userid = $(this).attr("value");

  $("#myModal").attr("value",userid);
  $.ajax({
                      url: "worker/callfunctions.php",
                      type: "POST",
                      cache: false,
                      data: {functionname:"get_message_box",userid:userid},
                     
                      success: function(datax){
                        $("#mod-content").html(datax);
                        }
                      
                    });
})


// To prompt user before unload  

  var show_close_alert = true;
    $(window).on("beforeunload", function() {
       // if (show_close_alert) {
            return "Killing me won't bring her back...";
        
    });



$("#book_title").on( "click", function(e){
        e.preventDefault();
        var d = new Date();
        var n = d.getSeconds();
        var id = (n<10) ? $(this).attr("value") + "0" + n : $(this).attr("value") + n;
        var url = "viewer.php?id=" + id;
        window.open(url);
});   

$('#poststatus').click(function(e){
        e.preventDefault();
        var statustext = $("#statustextarea").val();
        if(!statustext == ""){
             $.post("poststat.php",{statustext:statustext},function(datax){
                     $currentstatus=datax;
                     $("#statustextarea").val('');
                     $('#currstat').html($currentstatus);
             });
        }
});    

$('#viewstatus').click(function(e){
        e.preventDefault();
        var functionname = "showstatus";
        //alert("hello");
           $.post("callfunctions.php",{functionname:functionname},function(datax)
        {//
          //alert(datax);
         if( $('#viewstatus').html()== "View all")
        { $('#viewstatus').html("Hide");
          $("#showstatus").html(datax);$("#showstatus").css('display','block');
         
         }
         else
        { $('#viewstatus').html("View all");
          $("#showstatus").html(datax);$("#showstatus").css('display','none');
         
         }
        });
    
});
$(document).on("click","#submit_comment",function()
{
    var text = $('#message_body').val();
    var modal = $(this).closest("#myModal");
    var bookid = modal.attr("value");
    $('#message_body').val("");
    $('.counter_msg').html("");
    

                        if(!text || !bookid)
                            alert("somthing null :(");
                        else
                        { 
                            $.ajax({                
                                 url: "worker/callfunctions.php",
                                 type: "POST",
                                 data: {text: text,bookid: bookid,functionname:"insert_comment"},
                                 success: function(msg)
                                             {     
                                                   // $("#content").append('<center><br><a href ="#" id="read more" class = "">Read More</a></center>');
                                                   $('#comment_div').html(msg);
                                                   
                                                   
                                             }
                                    });
                        }
    });



$(document).on("click","#submit_message",function()
{
    var text = $('#message_body').val();
    var modal = $(this).closest("#myModal");
    var userid = modal.attr("value");
    $('#message_body').val("");
    $('.counter_msg').html("");
    $('.counter_msg').html("Message Sent !");
    setTimeout(function(){$('.counter_msg').html("");}, 1000);


                        if(!text || !userid)
                            alert("somthing null :(");
                        else
                        { 
                            $.ajax({                
                                 url: "worker/callfunctions.php",
                                 type: "POST",
                                 data: {text: text,userid: userid,functionname:"send_message"},
                                 success: function(msg)
                                             {     
                                                   // $("#content").append('<center><br><a href ="#" id="read more" class = "">Read More</a></center>');
                                                   
                                                   
                                                   
                                             }
                                    });
                        }
    });
   
   
$(document).on('click', '[name="follow_button"]',function(e)
    {
        e.preventDefault();
        var tobefollowed = $(this).attr('value');
        var followedby = parseInt($("#lu").html());
        var functionname = "follow";
   
        $this = $(this);
       var request = $.ajax({
            url : 'worker/callfunctions.php',
            type: 'POST',
            data: {functionname : functionname, followedby : followedby, tobefollowed : tobefollowed}
       });         
       request.done(function(msg){
        var follower_followers_count_div = ((($this).parent()).siblings("p")).find("#follower_followers_count");
        var follower_followers_count = parseInt(follower_followers_count_div.html());
        follower_followers_count_div.html(follower_followers_count+1);
   
       $this.attr('name','unfollow_button');
            $this.html('<span class="glyphicon glyphicon-heart"></span> Unfollow');
            $("#following_count").html(follower_followers_count+1);
            //$("#follow").css('pointer-events','none');
        });
       request.fail(function(jqXHR, textStatus) {
      alert( "Request failed: " + textStatus );
    });
  

    });

    $(document).on('click', '[name="unfollow_button"]',function(e)
    {
        e.preventDefault();
        var tobeunfollowed = $(this).attr('value');
        var followedby = parseInt($("#lu").html());
        var functionname = "unfollow";
   
        $this = $(this);
       var request = $.ajax({
            url : 'worker/callfunctions.php',
            type: 'POST',
            data: {functionname : functionname, followedby : followedby, tobeunfollowed : tobeunfollowed}
       });         
       request.done(function(msg){
       var follower_followers_count_div = ((($this).parent()).siblings("p")).find("#follower_followers_count");
        var follower_followers_count = parseInt(follower_followers_count_div.html());
        follower_followers_count_div.html(follower_followers_count-1); 
        $("#following_count").html(follower_followers_count-1);
         $this.attr('name','follow_button');
            $this.html('<span class="glyphicon glyphicon-heart"></span> Follow');
            //$("#follow").css('pointer-events','none');
        });
       request.fail(function(jqXHR, textStatus) {
      alert( "Request failed: " + textStatus );
    });
  

    });


$('#followers').click(function(e)
    {   $("#content_div").html();
        $("#content_div").html('<div class="panel panel-default"><div class="panel-body" id="followers_content"></div></div>');
        var s_limit = $("[name='as2']").length;
        var userid = parseInt(decodeURIComponent( (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,""])[1] )); //To get userid from url;
     //   var userid = (!$("#nu").html()) ? $("#lu").html() : $("#nu").html();
        var functionname = "viewfollowers";
        var request = $.ajax({
                            url : "worker/callfunctions.php",
                            type: "POST",
                            data: {functionname : functionname,userid : userid,s_limit:s_limit}
                            });
        request.done(function(msg)
        {     var msg = jQuery.parseJSON(msg);
              
                $("#followers_content").html(msg['reply']);
              // $("#content").append('<center><br><a href ="#" id="read more" class = "">Read More</a></center>');

        });
    });
$('#followings').click(function(e)
    { 
        $("#content_div").html();
        $("#content_div").html('<div class="panel panel-default"><div class="panel-body" id="followings_content"></div></div>');
        
        var s_limit = $("[name='as2']").length;
        var userid = parseInt(decodeURIComponent( (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,""])[1] )); //To get userid from url;
     //   var userid = (!$("#nu").html()) ? $("#lu").html() : $("#nu").html();
        var functionname = "viewfollowings";
        var request = $.ajax({
                            url : "worker/callfunctions.php",
                            type: "POST",
                            data: {functionname : functionname,userid : userid,s_limit:s_limit}
                            });
        request.done(function(msg)
        {     var msg = jQuery.parseJSON(msg);
                $("#followings_content").html(msg['reply']);
              // $("#content").append('<center><br><a href ="#" id="read more" class = "">Read More</a></center>');

        });
    });

$(document).on("click" , "#see_more_followers",function(e){
  $(this).css("display","none");
   var s_limit = $("[name='as2']").length;
        var userid = parseInt(decodeURIComponent( (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,""])[1] )); //To get userid from url;
     //   var userid = (!$("#nu").html()) ? $("#lu").html() : $("#nu").html();
        var functionname = "viewfollowers";
        var request = $.ajax({
                            url : "worker/callfunctions.php",
                            type: "POST",
                            data: {functionname : functionname,userid : userid,s_limit:s_limit}
                            });
        request.done(function(msg)
        {     var msg = jQuery.parseJSON(msg);
                $("#followers_content").append(msg['reply']);
              // $("#content").append('<center><br><a href ="#" id="read more" class = "">Read More</a></center>');

        });
});
$(document).on("click" , "#see_more_followings",function(e){
  $(this).css("display","none");
  var s_limit = $("[name='as2']").length;
        var userid = parseInt(decodeURIComponent( (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,""])[1] )); //To get userid from url;
     //   var userid = (!$("#nu").html()) ? $("#lu").html() : $("#nu").html();
        var functionname = "viewfollowings";
        var request = $.ajax({
                            url : "worker/callfunctions.php",
                            type: "POST",
                            data: {functionname : functionname,userid : userid,s_limit:s_limit}
                            });
        request.done(function(msg)
        {     var msg = jQuery.parseJSON(msg);
              
                $("#followings_content").append(msg['reply']);
              // $("#content").append('<center><br><a href ="#" id="read more" class = "">Read More</a></center>');

        });
});



$(document).on('focus keyup', '#message_body', function (e) {
  
    var $this = $(this);
    var msgSpan = $this.siblings('.counter_msg');
    var ml = parseInt($this.attr('maxlength'), 10);
    var length = this.value.length;
    var msg = ml - length + ' characters of ' + ml + ' characters left';

    msgSpan.html(msg);
});


/*$(document).on('click','#send_message',function(e)
{
  e.preventDefault;
  var msg = $('#message_body').val();
  msg = msg.replace("'", "\'");
    var msg_to = $('#msg_to').html();
        var functionname = "send_message";
        var request = $.ajax({
                            url : "worker/callfunctions.php",
                            type: "POST",
                            data: {functionname : functionname,msg : msg,msg_to:msg_to}
                            });
        request.done(function(msg)
        {     var msg = jQuery.parseJSON(msg);
                $("#followers_content").append(msg['reply']);
              // $("#content").append('<center><br><a href ="#" id="read more" class = "">Read More</a></center>');

        });

  alert(msg); 
})*/

$("#get_collections,#get_favourites,#get_wishlist,#get_read").on("click",function()
{
      var functionname = "get_shelf_items";
      var userid = parseInt(decodeURIComponent( (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,""])[1] )); //To get userid from url;
      var required_shelf = $(this).attr("id");
      $.ajax({
              url: "worker/callfunctions.php",
              type: "POST",
              cache: false,
              data: {functionname:"get_shelf_items",userid:userid,required_shelf:required_shelf},
              success: function(html){
                //j("#recepient_list").html(html);
                           switch(required_shelf){
                               case "get_collections" : $("#collection").html(html);break;
                               case "get_favourites" : $("#favourites").html(html);break;
                               case "get_wishlist" : $("#wishlist").html(html);break;
                               case "get_read" : $("#reading").html(html);break;
                            }
                }
            })
});
$(document).on("click","#add_to_collections,#add_to_favourites,#add_to_wishlist",function()
{
      var functionname = "add_to_shelf";
      var required_shelf = $(this).attr("id");
      var tile = $(this).closest("[name='book_stream']");
      var bookid = tile.attr("value");
      switch(required_shelf){
                               case "add_to_collections" : $(this).html('<span class="pull-right fa fa-check"></span> Collections');
                                                            tile.find("[name='notice']").html("Added To Collections");
                                                            tile.find("[name='notice']").css("display","block");
                                                            tile.find("[name='notice']").fadeOut( 2000, function() {});
                                                            break;
                               case "add_to_favourites" : $(this).html('<span class="pull-right fa fa-check"></span> Favourites');
                                                            tile.find("[name='notice']").html("Added To Favourites");
                                                            tile.find("[name='notice']").css("display","block");
                                                            tile.find("[name='notice']").fadeOut( 2000, function() {});
                                                            break;
                               case "add_to_wishlist" : $(this).html('<span class="pull-right fa fa-check"></span> Wishlist');
                                                            tile.find("[name='notice']").html("Added To Wishlist");
                                                            tile.find("[name='notice']").css("display","block");
                                                            tile.find("[name='notice']").fadeOut( 2000, function() {});
                                                            break;
                            }
      $.ajax({
              url: "worker/callfunctions.php",
              type: "POST",
              cache: false,
              data: {functionname:functionname,required_shelf:required_shelf,bookid:bookid},
              success: function(html){
                //j("#recepient_list").html(html);
                           
                }
            })
});
$(document).on("click","#remove_from_collections,#remove_from_favourites,#remove_from_wishlist",function()
{
      var functionname = "remove_from_shelf";
      var required_shelf = $(this).attr("id");
      var tile = $(this).closest("[name='book_stream']");
      var bookid = tile.attr("value");
      switch(required_shelf){
                               case "remove_from_collections" : $(this).html('<span class="pull-right fa fa-check"></span> Collections');
                                                            tile.find("[name='notice']").html("Removed from Collections");
                                                            tile.find("[name='notice']").css("display","block");
                                                            tile.find("[name='notice']").fadeOut( 2000, function() {});
                                                            break;
                               case "remove_from_favourites" : $(this).html('<span class="pull-right fa fa-check"></span> Favourites');
                                                            tile.find("[name='notice']").html("Removed from Favourites");
                                                            tile.find("[name='notice']").css("display","block");
                                                            tile.find("[name='notice']").fadeOut( 2000, function() {});
                                                            break;
                               case "remove_from_wishlist" : $(this).html('<span class="pull-right fa fa-check"></span> Wishlist');
                                                            tile.find("[name='notice']").html("Removed from Wishlist");
                                                            tile.find("[name='notice']").css("display","block");
                                                            tile.find("[name='notice']").fadeOut( 2000, function() {});
                                                            break;
                            }

      $.ajax({
              url: "worker/callfunctions.php",
              type: "POST",
              cache: false,
              data: {functionname:functionname,required_shelf:required_shelf,bookid:bookid},
              success: function(html){
                //j("#recepient_list").html(html);
                 tile.css("display","none");          
                }
            });
});
/*$(document).on("mouseover","#add_to_collections,#add_to_favourites,#add_to_wishlist",function()
{
      var this = $(this);
      var new_span = $('<span name = "add">Add</span>').addClass('pull-right fa fa-check');
      new_span.appendTo(this);
});
$(document).on("mouseout","#add_to_collections,#add_to_favourites,#add_to_wishlist",function()
{
      $($(this).find("span")).css("display","none");
});*/



/*USer suggestio*/
$(document).on("click", "[name='close_suggestion']", function(){
  var x = $(this).closest(".panel").find(".close").length;

$(this).closest(".media").remove();
if(x<=2)
{var limit = $("#user_suggestions").attr("value")+1;
        $.ajax({
              url: "worker/callfunctions.php",
              type: "POST",
              cache: false,
              data: {functionname:"get_user_suggestions",limit:limit},
              success: function(msg){
                var msg = jQuery.parseJSON(msg);
                $("#user_suggestions").append(msg['reply']);
                $("#user_suggestions").attr("value",msg['limit']);
                //j("#recepient_list").html(html);
                         
                }
            });
}

});

/*-/ user suggestion*/
  