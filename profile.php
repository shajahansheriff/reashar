<?php
   
    require_once("worker/cake/models/config.php");
    if(!isUserLoggedIn()) { header("Location: index.php"); die(); }

    //$username = $loggedInUser->username;
    $user_id = (int)$_GET['id']; 
    $userdetails = fetchUserDetailsById($user_id);
    //foreach ($userdetails as $key => $value) {
    // echo $key."=>".$value;
   //}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Note there is no responsive meta tag here -->

    <link rel="shortcut icon" href="img/favicon.png">

    <title>Welcome | Reashar</title>

    <!-- Bootstrap core CSS -->
    <link href="css/global.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/non-responsive.css" rel="stylesheet">
    <link href="css/animate-custom.css" rel="stylesheet" />
    <link href="css/loginbox.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
     <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <?php 
    include'navbar.php' ?>
    <?php echo '<div id ="lu" style ="display:none;">'.$loggedInUser->user_id.'</div>'; ?>
   
  
    
    <div class="container">
 
   <div class="col-xs-3" >
        <div class="panel panel-default">
          <div class="panel-heading"><a href="#" class="pull-right" style="font-size: 12px" id = "edit_profile"> edit</a> <h4>Profile</h4></div>
   			   <div class="panel-body profile-body">
              
              	<div >
                  <img id="profile_pic" src="<?php if(isset($userdetails['profile_pic'])) echo $userdetails['profile_pic']; else echo "photos/avatar.jpg";?> " class="profile-pic">
                  </div>
                   <p style="text-align:center">
                            <a href="#" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-heart"></span> Follow</a>
                            <a href="#" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-comment"></span> Message</a>
                         </p>
               <ul class="profile-ul">
                <li><b><?php echo $userdetails['fname']."&nbsp;".$userdetails['lname'];?></b></li>
                <li><b>Job: </b><?php if(isset($userdetails['occupation'])) echo " ".$userdetails['occupation']; else echo " None";?></li>
                <li><b>Lives:</b><span style="display:inline-block"><?php if(isset($userdetails['location'])) echo " ".$userdetails['location']; else echo " None";?></span></li>
                <li><a href="#">See more</a></li>
              </ul>
        
              
    
                	
            </div>

   	    </div>
        <div class="list-group">
                <a id = "followers" class = "list-group-item"  href="#"><b>Followers</b><span class="badge" id="follower_count"><?php echo $userdetails['followercount']; ?> </span></a>
                <a id = "followings" class = "list-group-item"  href="#"><b>Following</b><span class="badge" id="following_count"><?php echo $userdetails['followingcount']; ?> </span></a> 
                <a id = "followers" class = "list-group-item"  href="#"><b>Shelf  </b></a>
                <a href="#" class="list-group-item">Current Reading <span class="badge">120</span></a>
<div id="lu" class = "hidden_item"><?php if(isset($loggedInUser->user_id)) echo $loggedInUser->user_id; echo '';?></div>
    
              </div>  
        </div>
 
  <div class="col-xs-9">
<div class="panel panel-default shelf">
                        <div class="panel-heading">
                            <i class="fa fa-th"></i> My Shelf
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

   <!-- Nav tabs -->
<div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-pills nav-shelf">
                                <li class="active "><a href="#mybooks"  data-toggle="tab">My Books</a>
                                </li>
                                <li><a href="#reading" data-toggle="tab">Reading</a>
                                </li>
                                <li><a href="#collection" data-toggle="tab">collections</a>
                                </li>
                                <li><a href="#favourites" data-toggle="tab">Favourites</a>
                                </li>
                                <li><a href="#wishlist" data-toggle="tab">Wishlist</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="mybooks">
                                    <div class="col-xs-4 stream-item"><div class="panel panel-default">
          <div class="panel-heading panel-heading-no-pad"><p class="panel-head"><a href="#" id="" class="stream-link">The Godfather shajahan 1qwertyuas yr</a></p></div>
          <a href="#" id=""><img src="photos/h.jpg" class="img-panel"></a>
              
            <div class="panel-footer book-category"><small><i >Category:</i> Comics, Thriller Comics, Thriller</small></div>
                                                            <div class="panel-footer"><div class="pull-right btn-group">
                                                <p href="" class="pull-right glyphicon glyphicon-plus-sign dropdown-toggle" data-toggle="dropdown">
</p>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Wishlist</a>
                                                    </li>
                                                    <li><a href="#">Collection</a>
                                                    </li>
                                                    <li><a href="#">Favourites</a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <a href="#" id="" class="glyphicon glyphicon-heart pull-left"></a><p class="count">143</p>
              
            </div>
          </div>
        </div>
        <div class="col-xs-4 stream-item"><div class="panel panel-default">
          <div class="panel-heading panel-heading-no-pad"><p class="panel-head"><a href="#" id="" class="stream-link">The Godfather shajahan 1qwertyuas yr</a></p></div>
          <a href="#" id=""><img src="photos/h.jpg" class="img-panel"></a>
              
            <div class="panel-footer book-category"><small><i >Category:</i> Comics, Thriller Comics, Thriller</small></div>
                                                            <div class="panel-footer"><div class="pull-right btn-group">
                                                <p href="" class="pull-right glyphicon glyphicon-plus-sign dropdown-toggle" data-toggle="dropdown">
</p>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Wishlist</a>
                                                    </li>
                                                    <li><a href="#">Collection</a>
                                                    </li>
                                                    <li><a href="#">Favourites</a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <a href="#" id="" class="glyphicon glyphicon-heart pull-left"></a><p class="count">143</p>
              
            </div>
          </div>
        </div>
        <div class="col-xs-4 stream-item"><div class="panel panel-default">
          <div class="panel-heading panel-heading-no-pad"><p class="panel-head"><a href="#" id="" class="stream-link">The Godfather shajahan 1qwertyuas yr</a></p></div>
          <a href="#" id=""><img src="photos/h.jpg" class="img-panel"></a>
              
            <div class="panel-footer book-category"><small><i >Category:</i> Comics, Thriller Comics, Thriller</small></div>
                                                            <div class="panel-footer"><div class="pull-right btn-group">
                                                <p href="" class="pull-right glyphicon glyphicon-plus-sign dropdown-toggle" data-toggle="dropdown">
</p>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Wishlist</a>
                                                    </li>
                                                    <li><a href="#">Collection</a>
                                                    </li>
                                                    <li><a href="#">Favourites</a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <a href="#" id="" class="glyphicon glyphicon-heart pull-left"></a><p class="count">143</p>
              
            </div>
          </div>
        </div>
        <div class="col-xs-4 stream-item"><div class="panel panel-default">
          <div class="panel-heading panel-heading-no-pad"><p class="panel-head"><a href="#" id="" class="stream-link">The Godfather shajahan 1qwertyuas yr</a></p></div>
          <a href="#" id=""><img src="photos/h.jpg" class="img-panel"></a>
              
            <div class="panel-footer book-category"><small><i >Category:</i> Comics, Thriller Comics, Thriller</small></div>
                                                            <div class="panel-footer"><div class="pull-right btn-group">
                                                <p href="" class="pull-right glyphicon glyphicon-plus-sign dropdown-toggle" data-toggle="dropdown">
</p>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Wishlist</a>
                                                    </li>
                                                    <li><a href="#">Collection</a>
                                                    </li>
                                                    <li><a href="#">Favourites</a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <a href="#" id="" class="glyphicon glyphicon-heart pull-left"></a><p class="count">143</p>
              
            </div>
          </div>
        </div>
                                    
                                </div>

                                <div class="tab-pane fade" id="reading">
                                    <h4>Reading</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                                <div class="tab-pane fade" id="collection">
                                    <h4>Collection</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                                <div class="tab-pane fade" id="favourites">
                                    <h4>Favourites</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                                <div class="tab-pane fade" id="wishlist">
                                    <h4>Wishlist</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </div>
                        </div>

   
    <div id="content_div">
      
    </div>
  </div>
</div>
  </div>

          

   
    

    <div class="col-xs-8" id = "edit_profile_div" style="display:none;width:100%">
	         <div class="panel panel-default">
              <div class="panel-heading"><a href="#" class="pull-right" style="font-size: 12px" id = "close_profile_div">Close</a> <h4>Edit Profile</h4></div>
              <div class="panel-body">
                    <?php include 'profile_div.php'; ?>
              </div> <!-- Panel body -->
            </div> <!-- Panel -->
   	</div>
    <div class="col-xs-8" id = "edit_profile_pic_div" style="display:none;width:100%">
            <div class="panel panel-default" id = "profile_pic_panel">
             <div class="panel-heading"><a href="#" class="pull-right" style="font-size: 12px" id = "close_profile_pic_div">Close</a> <h4>Edit Profile Picture</h4></div>
             <div class="panel-body">


             
                  <link href="css/dropzone.css" type="text/css" rel="stylesheet" />
                  <div  class="dropzone_type" value = "profile_picture"></div>
                  <center><form class="dropzone" ></form></center>
     
            </div> <!-- Panel body -->
          </div> <!-- Panel -->
    </div> <!-- /container -->
   



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/dropzone.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <script src="js/global.min.js"></script>
    <script src="js/jqueryfxns.js"></script>
    <script src="js/jquery.geocomplete.js"></script>
    <script type="text/javascript">
      $(function() {
        $('#asd').click(function() {
        $(".media-body").toggle(20);
        });
      });
      $(function(){
        $("#location").geocomplete()
      });
    </script>
    
    
    
   
    
    
  </body>
</html>
