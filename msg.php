
<?php

    require_once("worker/cake/models/config.php");  
    if(!isUserLoggedIn()) { header("Location: index.php"); die(); }

?>
      


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Note there is no responsive meta tag here -->

    <link rel="shortcut icon" href="img/favicon.png">

    <title>Welcome | Reashar</title>
        <!-- Bootstrap core CSS -->
    <link href="css/global.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/non-responsive.css" rel="stylesheet">
    <link href="css/animate-custom.css" rel="stylesheet" />
    <link href="css/loginbox.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />


  </head>

  <body>
    <div id ="recepient_list" style = "width:15%;height:600px;display:inline-block;vertical-align:top">
 </div>
<div id = "msg" style = "display:inline-block;width:65%">
<br>



<span id ="msg_to">1000000016</span>
   
<!-- <input name="sender" type="text" id="texta" value=""/> -->
<div class="refresh" style = "overflow:auto;height:225px">


</div>
            <br/>
            <textarea id="message_body" maxlength="500" autofocus name="sam_notes" style="height:100px;width:90%;margin:0 auto;"></textarea>
            <br/> 
         

<span class='counter_msg'></span><br><input type = "submit" value ="submit" id = "submit_message"></input><span id ="wait"></span>
<div id="footer" style="margin-top:50px;">
      
    </div>
  </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <script src="js/jquery.js"></script>
    <script src="js/jqueryfxns.js"></script>
  
<script language="javascript" src="js/jquery-1.2.6.min.js"></script> 
<script language="javascript" src="js/jquery.timers-1.0.0.js"></script>
<script src="js/timezone.js"></script>
<script type="text/javascript">



$('#msg_to').ready(function(){
   var j = jQuery.noConflict();
    $('#msg_to').ready(function()
    {
        j.ajax({
              url: "worker/refresh2.php",
              type: "POST",
              cache: false,
              data: {functionname: "messaged_users"},
              success: function(html){
                j("#recepient_list").html(html);
              }
            })

      
        j(".refresh").everyTime(5000,function(i){
            var msg_to = parseInt(j('#msg_to').html());
            var timezone = jstz.determine().name(); 
            if(!msg_to) alert("somthing null you idiot  :(");
                 else
                 { 
            j.ajax({
              url: "worker/refresh.php",
              type: "POST",
              cache: false,
              data: {msg_to: msg_to, timezone: timezone},
              success: function(html){
                j(".refresh").html(html);
              }
            })
            $.ajax({
              url: "worker/refresh2.php",
              type: "POST",
              cache: false,
              data: {functionname:""},
              success: function(datax){
                //j("#recepient_list").html(html);
                datax = JSON.parse(datax);
                for(var i=0;i<=datax['id'].length;i++)
                {$obj ="#"+datax['id'][i];
                  $($($obj)).find("#unseen_count").html(datax['unseen_count'][i]);
                }
              }
            })
             

          }
        })
        
    });
    $("#msg").click(function()
                {         
                     var msg_to = parseInt(j('#msg_to').html());
                     var timezone = jstz.determine().name(); 
            
            j.ajax({
              url: "worker/save.php",
              type: "POST",
              cache: false,
              data: {msg_to: msg_to, timezone: timezone, functionname: "seen"},
              success: function(html){
                j(".refresh").html(html);
              }
            });

     });
    $(document).bind("change", "#msg_to", function()
    {
            var msg_to = parseInt(j('#msg_to').html());
            var timezone = jstz.determine().name(); 
            if(!msg_to) alert("Select a person for whom the message hs to be sent");
                 else
                 { 
            j.ajax({
              url: "worker/refresh.php",
              type: "POST",
              cache: false,
              data: {msg_to: msg_to, timezone: timezone},
              success: function(html){
                j(".refresh").html(html);
              }
            })}
        
        
    });
    $(document).bind('focus keyup', '#message_body', function (e) {
  
    var $this = $(this);
    var msgSpan = $this.siblings('.counter_msg');
    var ml = parseInt($this.attr('maxlength'), 10);
    var length = $this.value.length;
    var msg = ml - length + ' characters of ' + ml + ' characters left';

    msgSpan.html(msg);
});

    j('#submit_message').click(function() {
                var text = j('#message_body').val();

                var msg_to = parseInt(j('#msg_to').html());

                if(!text || !msg_to)
                    alert("somthing null :(");
                else
                { 
                    j.ajax({                
                         url: "worker/save.php",
                         type: "POST",
                         data: {text: text,msg_to: msg_to},
                         success: function(msg)
                                     {     
                                           // j("#content").append('<center><br><a href ="#" id="read more" class = "">Read More</a></center>');
                                           j('.refresh')
                                           j('#message_body').val("");
                                           j('.counter_msg').value = "";
                                            $("#msg_to").change();
                                     }
                            });
                }
    });
    
           
        
   j('.refresh').css({color:"green"});
});
</script>
  </body>
</html>
