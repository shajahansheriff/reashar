<?php

require_once("worker/cake/models/config.php");
if (!securePage($_SERVER['PHP_SELF'])){die();}
echo isUserLoggedIn();
//Prevent the user visiting the logged in page if he/she is already logged in.... 
if(isUserLoggedIn()) { header("Location: home.php"); die(); }

      

//Forms posted
if(!empty($_POST))
{
  $errors = array();
  $username = sanitize(trim($_POST["username"]));
  $password = trim($_POST["password"]);
  
  //Perform some validation
  //Feel free to edit / change as required
  if($username == "")
  {
    $errors[] = lang("ACCOUNT_SPECIFY_USERNAME");
  }
  if($password == "")
  {
    $errors[] = lang("ACCOUNT_SPECIFY_PASSWORD");
  }

  if(count($errors) == 0)
  {
    //A security note here, never tell the user which credential was incorrect
    if(!usernameExists($username))
    {
      $errors[] = lang("ACCOUNT_USER_OR_PASS_INVALID");
    }
    else
    {
      $userdetails = fetchUserDetails($username);
      //See if the user's account is activated
      if($userdetails['active']==0)
      {
        $errors[] = lang("ACCOUNT_INACTIVE");
      }
      else
      {
        //Hash the password and use the salt from the database to compare the password.
        $entered_pass = generateHash($password,$userdetails["password"]);
       // echo $entered_pass."<br>".$userdetails["password"];
    
        if($entered_pass != $userdetails["password"])
        {
          //Again, we know the password is at fault here, but lets not give away the combination incase of someone bruteforcing
          $errors[] = lang("ACCOUNT_USER_OR_PASS_INVALID");
        }
        else
        {//echo "hello";
         //echo "Passwords match! we're good to go'";
          //
          //Construct a new logged in user object
          //Transfer some db data to the session object
          
          $loggedInUser = new loggedInUser();
          // $_SESSION["userCakeUser"] = new loggedInUser();
           $loggedInUser->email = $userdetails["email"];
           $loggedInUser->user_id = $userdetails["id"];
           $loggedInUser->hash_pw = $userdetails["password"];
           //$loggedInUser->title = $userdetails["title"];
           //$loggedInUser->displayname = $userdetails["display_name"];
           $loggedInUser->username = $userdetails["user_name"];
           $loggedInUser->fname = $userdetails["fname"];
           //Update last sign in
           $loggedInUser->updateLastSignIn();
           //$_SESSION["userCakeUser"] = $loggedInUser;
           
                     
                         $_SESSION["reasharusername"] =   $loggedInUser->username;
                         $_SESSION["reasharfname"]    =   $loggedInUser->fname;
                         $_SESSION["reasharuserid"]   =   $loggedInUser->user_id;
                         $_SESSION["reasharemail"]    =   $loggedInUser->email;


                         //$_SESSION["email"] = $userdetails["email"];
                         //$_SESSION["user_id"] = $userdetails["id"];
                         
                        // $_SESSION["username"] = $userdetails["user_name"];
                         //echo "hello";
                         //echo $_SESSION["ReasharUser"];
                         //Redirect to user account page
                      //  echo $_SESSION["userCakeUser"]->username;
                        header("Location: home.php");
                      
         //echo $_SESSION["reasharusername"];

          die();
        }
      }
    }
  }
}?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Note there is no responsive meta tag here -->

    <link rel="shortcut icon" href="img/favicon.png">

    <title>Welcome | Reashar</title>

    <!-- Bootstrap core CSS -->
    <link href="css/global.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/non-responsive.css" rel="stylesheet">
    <link href="css/animate-custom.css" rel="stylesheet" />
    <link href="css/loginbox.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <?php include'navbar.php'; ?>
    <div class="container">

     <div class="row">
       <div class=" col-xs-8 jumbotron">
       <center><h1>REASHAR</h1>
        <h3>READ ALL AT ONE PLACE</h3>
        <p>FROM BOOKS, MAGAZINES, NEWSPAPERS AND COMIC BOOKS</p>
        <a class="btn btn-primary btn-lg"  style="border-radius:25px; margin-top:15px;"href="about.html">Know more &nbsp;<span class="glyphicon glyphicon-circle-arrow-right"></span></a>
        </center>
      	</div>
      
<div class="col-xs-4 loginbox">
	     <div id="container_demo" >
                    <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper">
                      <!-- <div -->
                        <div id="login" class="animate form">
                            <form  action="" autocomplete="on" method="post"> 
                                
                                <p> 
                                    <label for="username" class="uname" data-icon="u" > Your email or username </label>
                                    <input id="username" name="username" required="required" type="text" placeholder="username or mymail@mail.com"/>
                                </p>
                                <p> 
                                    <label for="password" class="youpasswd" data-icon="p"> Your password </label>
                                    <input id="password" name="password" required="required" type="password" placeholder="eg. X8df!90EO" /> 
                                </p>

                                <p class="keeplogin"> 
									<input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping" /> 
									<label for="loginkeeping">Keep me logged in</label>
                  <p><?php echo resultBlock($errors,$successes); ?></p>
								</p>
                                <p class="login button"> 
                                    <input type="submit" value="Login" /> 
								</p>
                                <p class="change_link">
									Not a member yet ?
									<a href="#toregister" class="to_register">Join us</a>
								</p>
                            </form>
                        </div>

                        <div id="register" class="animate form">
                            <form  action="mysuperscript.php" autocomplete="on"> 
                                
                                <p> 
                                    <label for="usernamesignup" class="uname" data-icon="u">New username</label>
                                    <input id="usernamesignup" name="usernamesignup" required="required" type="text" placeholder="mysuperusername690" />
                                </p>
                                <p> 
                                    <label for="emailsignup" class="youmail" data-icon="e" > Your email</label>
                                    <input id="emailsignup" name="emailsignup" required="required" type="email" placeholder="mysupermail@mail.com"/> 
                                </p>
                                <p class="alert-danger">email is already registered</p>
                                <p> 
                                    <label for="passwordsignup" class="youpasswd" data-icon="p">New password </label>
                                    <input id="passwordsignup" name="passwordsignup" required="required" type="password" placeholder="eg. X8df!90EO"/>
                                </p>
                                <p> 
                                    <label for="passwordsignup_confirm" class="youpasswd" data-icon="p">Please confirm your password </label>
                                    <input id="passwordsignup_confirm" name="passwordsignup_confirm" required="required" type="password" placeholder="eg. X8df!90EO"/>
                                </p>
                                <p class="signin button"> 
									<input type="submit" value="Sign up"/> 
								</p>
                                <p class="change_link">  
									Already a member ?
									<a href="#tologin" class="to_register"> Go and log in </a>
								</p>
                            </form>
                        </div>
						
                    </div>
                </div>  

</div>

     <!-- <h3>What changes</h3>
      <p>Note the lack of the <code>&lt;meta name="viewport" content="width=device-width, initial-scale=1.0"&gt;</code>, which disables the zooming aspect of sites in mobile devices. In addition, we reset our container's width and are basically good to go.</p>

      <h3>Regarding navbars</h3>
      <p>As a heads up, the navbar component is rather tricky here in that the styles for displaying it are rather specific and detailed. Overrides to ensure desktop styles display are not as performant or sleek as one would like. Just be aware there may be potential gotchas as you build on top of this example when using the navbar.</p>

      <h3>Non-responsive grid system</h3>
      <div class="row">
        <div class="col-xs-4">One third</div>
        <div class="col-xs-4">One third</div>
        <div class="col-xs-4">One third</div>
      </div>
-->

   </div> </div> <!-- /container -->

<div id="footer" style="margin-top:50px;">
      <div class="container">
        <p class="text-muted credit">Reashar <b class="glyphicon glyphicon-copyright-mark"></b> 2014 <a href="#">About</a> | <a href="#">Privacy</a> | <a href=""></b>Terms and Condition</a></p>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
